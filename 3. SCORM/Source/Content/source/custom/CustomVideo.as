package source.custom
{
import flash.display.*;
import flash.events.*;
import flash.filters.*;
import flash.geom.*;
import flash.net.*;
import flash.text.*;

import mx.controls.*;
import mx.core.*;
import mx.events.MetadataEvent;
import mx.events.SliderEvent;
import mx.events.VideoEvent;

import com.fxcomponents.controls.fxvideo.*;

/*
	Based loosely upon the fxcomponents FXVideo class
	Provides transport controls for Flex's VideoDisplay class
	Far from perfect, but perfectly usable
*/

public class CustomVideo extends VideoDisplay
{
	include "Functions.as"

	[Embed(source="../../bin/skins/button_lightbox_video_play.swf")]
  		public var PlayButton:Class;
	[Embed(source="../../bin/skins/button_lightbox_video_pause.swf")]
  		public var PauseButton:Class;
	[Embed(source="../../bin/skins/button_lightbox_video_mute.swf")]
  		public var MuteButton:Class;
	[Embed(source="../../bin/skins/button_lightbox_video_fullscreen.swf")]
  		public var FullScreenButton:Class;

	public function CustomVideo() 
	{
		super();
		
		textFormat = new TextFormat();
	}

	private var textFormat:TextFormat;
	private var thumbHookedToPlayhead:Boolean = true;
	private var volumeBeforeMute:Number = 0;
	private var loadProgress:Number = 0;
	private var is_first_time:Boolean = true;
	private var use_hilo : Boolean = false;
	private var use_fullscreen : Boolean = true;
	private var base_url : String = "0";

	/** display objects */

	private var controlBar:UIComponent;
	private var playheadSlider:CustomVideoScrubber;
	private var volumeSlider:CustomVideoScrubber;
	private var videoArea:UIComponent;
	private var playButton:UIComponent;
	private var soundButton:UIComponent;
	private var timerTextField:TextField;
	private var fullScreenButton:UIComponent;
	private var hiloButton:HiLoButton;
	private var pause_icon:Sprite;
	private var play_icon:Sprite;

	/** style */

	private var _frontColor:uint = 0x808080;
	private var _backColor:uint = 0x303030;
	private var _controlBarHeight:uint = 20;
	private var _timerFontName:String = "Verdana";
	private var _timerFontSize:Number = 9;
	private var button_radius:Number = 9;

	private var spinner_sprite:Sprite = null;
	private var spinner_alpha:Number = 0.0;

	public var complete_handler:* = null;
	public var pause_handler:Function = null;
	public var play_handler:Function = null;

	private var _adjustVolumeOnScroll:Boolean = true;
	public function set adjustVolumeOnScroll(value:Boolean):void { _adjustVolumeOnScroll = value; }
	public function get adjustVolumeOnScroll():Boolean { return _adjustVolumeOnScroll; }

	private var _playPressed:Boolean;
	private function set playPressed(value:Boolean):void
	{
		_playPressed = value;
		play_icon.visible = !value;
		pause_icon.visible = value;
	}

	private function get playPressed():Boolean
	{
		return _playPressed;
	}

	override protected function createChildren():void
	{
		super.createChildren();

		addEventListener(MetadataEvent.METADATA_RECEIVED, onMetadataReceived);
		addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
		addEventListener(ProgressEvent.PROGRESS, onProgress);
		addEventListener(VideoEvent.PLAYHEAD_UPDATE, onPlayheadUpdate);
		addEventListener(VideoEvent.STATE_CHANGE, onStateChange);
		addEventListener(VideoEvent.REWIND, onRewind);
		addEventListener(VideoEvent.COMPLETE, onComplete);
		addEventListener(VideoEvent.READY, onReady);

		videoArea = new UIComponent();
		//videoArea.addEventListener(MouseEvent.CLICK, full_screen_onClick);
		addChild(videoArea);

		controlBar = new UIComponent();
		addChild(controlBar);

		playheadSlider = new CustomVideoScrubber();
		playheadSlider.addEventListener(SliderEvent.CHANGE, playhead_onChange);
		playheadSlider.addEventListener(SliderEvent.THUMB_PRESS, onThumbPress);
		playheadSlider.addEventListener(SliderEvent.THUMB_RELEASE, onThumbRelease);
		playheadSlider.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		playheadSlider.addEventListener(SliderEvent.THUMB_DRAG, onThumbDrag);
		controlBar.addChild(playheadSlider);

		volumeSlider = new CustomVideoScrubber();
		volumeSlider.addEventListener(SliderEvent.CHANGE, volume_onChange);
		controlBar.addChild(volumeSlider);

		////////////////////////////////

		playButton = new UIComponent();
		play_icon = new PlayButton;
		pause_icon = new PauseButton;
		playButton.addChild (play_icon);
		playButton.addChild (pause_icon);
		playButton.addEventListener(MouseEvent.CLICK, ClickPlayPause);
		playButton.buttonMode = true;
		controlBar.addChild (playButton);

		//////////////////////////

		soundButton = new UIComponent();
		soundButton.addEventListener(MouseEvent.CLICK, volume_onClick);
		var sound_icon : Sprite = new MuteButton;
		soundButton.addChild (sound_icon);
		soundButton.buttonMode = true;
		controlBar.addChild (soundButton);
		
		//////////////////////////

		timerTextField = new TextField();
		controlBar.addChild(timerTextField);

		if (use_hilo){
			hiloButton = new HiLoButton();
			hiloButton.addEventListener(MouseEvent.CLICK, hi_lo_onClick);
			controlBar.addChild (hiloButton);}

		if (use_fullscreen){
			fullScreenButton = new UIComponent();
			fullScreenButton.addChild (new FullScreenButton);
			fullScreenButton.addEventListener(MouseEvent.CLICK, full_screen_onClick);
			controlBar.addChild (fullScreenButton);}

        (autoPlay) ? playPressed = true : playPressed = false
	}

	override protected function commitProperties():void
	{
		super.commitProperties();

		//playButton.iconColor = _frontColor;
        //volumeButton.iconColor = _frontColor;

		volumeSlider.maximum = 1;
		volumeSlider.value = volume;

		textFormat.color = _frontColor;
		textFormat.font = _timerFontName;
		textFormat.size = _timerFontSize;

		timerTextField.defaultTextFormat = textFormat;
		timerTextField.text = "";
		timerTextField.selectable = false;
		timerTextField.autoSize = TextFieldAutoSize.LEFT;
	}

	override protected function updateDisplayList (unscaledWidth:Number, unscaledHeight:Number):void
    {
		var aspect_width : int = unscaledHeight * 16 / 9;
		if (aspect_width < unscaledWidth) unscaledWidth = aspect_width;
		else unscaledHeight = unscaledWidth * 9 / 16;

        super.updateDisplayList(unscaledWidth, unscaledHeight);

        var h:uint = _controlBarHeight;

        // draw

        videoArea.graphics.clear();
        videoArea.graphics.beginFill(0, 0);
        videoArea.graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);

        controlBar.graphics.clear();
		//controlBar.graphics.beginFill(_backColor);
		var mat:Matrix = new Matrix();
  		mat.createGradientBox (unscaledWidth, h, Math.PI/2);
		controlBar.graphics.beginGradientFill ("linear", [0x282828, 0x3A3A3A, 0x222222], [1, 1, 1], [0, 100, 255], mat);
		controlBar.graphics.drawRect(0, 0, unscaledWidth, h);
		controlBar.graphics.endFill();

        // size

        var hiloButton_width:int = use_hilo ? hiloButton.width : 0;
        var fullScreenButton_width:int = use_fullscreen ? fullScreenButton.width : 0;

        controlBar.setActualSize(unscaledWidth, h);
        playButton.setActualSize(h, h);
        soundButton.setActualSize(h, h);
        playheadSlider.setActualSize(unscaledWidth - playButton.width - soundButton.width - hiloButton_width - fullScreenButton_width - 170, 9);
        volumeSlider.setActualSize(60, 9);
        if (use_hilo) hiloButton.setActualSize(h, h);
        if (use_fullscreen) fullScreenButton.setActualSize(h+12, h+8);

        // position

		controlBar.x = 0;
		controlBar.y = unscaledHeight - 1;

        playButton.x = 0;
        playButton.y = 0;

        playheadSlider.x = playButton.x + playButton.width + 10;
        playheadSlider.y = (controlBar.height - playheadSlider.height) / 2;

        timerTextField.x = playheadSlider.x + playheadSlider.width + 10;
        timerTextField.y = 4; //(controlBar.height - timerTextField.height)/2;

        soundButton.x = unscaledWidth - volumeSlider.width - soundButton.width - hiloButton_width - fullScreenButton_width - 14;
        soundButton.y = 0;

        volumeSlider.x = unscaledWidth - fullScreenButton_width - hiloButton_width - volumeSlider.width - 10;
        volumeSlider.y = playheadSlider.y;

		if (use_hilo){
        	hiloButton.x = unscaledWidth - fullScreenButton_width - hiloButton_width - 8;
        	hiloButton.y = 0;}

		if (use_fullscreen){
        	fullScreenButton.x = unscaledWidth - fullScreenButton_width;
        	fullScreenButton.y = 0;}

        if (!videoPlayer)
            createVideoPlayer();

        videoPlayer.x = controlBar.x = (this.width - videoPlayer.width) / 2;
        videoPlayer.y = (this.height - videoPlayer.height) / 2;
        controlBar.y = videoPlayer.y + videoPlayer.height;

		if (spinner_sprite){
			spinner_sprite.x = videoPlayer.width / 2;
			spinner_sprite.y = videoPlayer.height / 2;}
    }
    
	public function ClickPlayPause(event:MouseEvent):void
	{
		//Application.application.HidePlayButton();
		
		if(playPressed)
		{
			pause();
			playPressed = false;
			if (pause_handler != null) pause_handler();
		}
		else
		{
			play();
			playPressed = true;
			if (play_handler != null) play_handler();
		}
	}
	private function stop_onClick(event:MouseEvent):void
	{
		stop();
		playPressed = false;
	}
	private function volume_onClick(event:MouseEvent):void
	{
		if(volume == 0)
		{
			volume = volumeSlider.value = volumeBeforeMute;
		}
		else
		{
			volumeBeforeMute = volume;
			volume = volumeSlider.value = 0;
		}
	}
	private function onMouseDown(event:MouseEvent):void
	{
		thumbHookedToPlayhead = false;
	}
	private function onMouseWheel(event:MouseEvent):void
	{
		if(!_adjustVolumeOnScroll)
			return;
			
		volume += event.delta/Math.abs(event.delta)*.05;
		volumeSlider.value = volume
	}
	
	/** SliderEvent */
	
	private function onThumbPress(event:SliderEvent):void
	{
		thumbHookedToPlayhead = false;
	}
	private function onThumbRelease(event:SliderEvent):void
	{
		thumbHookedToPlayhead = true;
	}
	private function onThumbDrag(event:SliderEvent):void
	{
		timerTextField.text = formatTime(event.value)+" / "+formatTime(totalTime);
	}
	private function playhead_onChange(event:SliderEvent):void
	{
		thumbHookedToPlayhead = true;
		try
		{ 
			playheadTime = event.currentTarget.value;
		}
		catch (error:Error){}
	}
	private function volume_onChange(event:SliderEvent):void
	{
		volume = event.currentTarget.value;
	}
	
	private function onRewind(event:VideoEvent):void
	{
		
	}
	private function onMetadataReceived(event:MetadataEvent):void
	{
		playheadSlider.maximum = Math.round(totalTime);
		updateTimer();
		HideSpinner ();
	}
	private function onPlayheadUpdate(event:VideoEvent):void
	{
		if (thumbHookedToPlayhead)
		{
			playheadSlider.value = Math.round(event.playheadTime);
			updateTimer();
		}
		
		if (is_first_time)
		{
			thumbHookedToPlayhead = false;
		}
		
		if (is_first_time && playheadTime > 0)
		{
			thumbHookedToPlayhead = true;
			is_first_time = false;
		}
	}

	private function onStateChange(event:VideoEvent):void
	{
		if (event.state == VideoEvent.CONNECTION_ERROR)
		{
			timerTextField.text = "Connection error";
		}
	}
	
	private function onReady(event:VideoEvent):void
	{
		HideSpinner ();
	}
	private function onComplete(event:VideoEvent):void
	{
		playPressed = false;
		stage["displayState"] = "normal";
		if (complete_handler) complete_handler.call (complete_handler);
	}
	private function onProgress(event:ProgressEvent):void
	{
		loadProgress = Math.floor(event.bytesLoaded/event.bytesTotal*100);
		var playheadProgress:Number = Math.floor(playheadTime/totalTime*100);
		//playheadSlider.progress = loadProgress;
	}

	/** functions */

	private function formatTime(value:int):String
	{
		var result:String = (value % 60).toString();
        if (result.length == 1)
            result = Math.floor(value / 60).toString() + ":0" + result;
        else 
            result = Math.floor(value / 60).toString() + ":" + result;
        return result;
	}

	private function formatVolume(value:Number):Number
	{
		var result:Number = Math.round(value*100);
		return result;
	}

	private function updateTimer():void
	{
		timerTextField.text = formatTime(playheadTime)+" / "+formatTime(totalTime);
		if (playheadTime > 1) HideSpinner();
	}

	private function full_screen_onClick (event:MouseEvent):void
	{
		var app:* = Application.application;
		if (app.hasOwnProperty ("view"))
		{
			if (stage.displayState == "normal")
			{
				var video_control:* = Object(parent.parent);
				if (video_control.hasOwnProperty ("FullScreen"))
					video_control.FullScreen ();
			}
			else
			{
				stage["displayState"] = "normal";
			}
		}
		else
		{
			if (stage.displayState == "normal")
			{
				stage["displayState"] = "fullScreen";
				stage.addEventListener ("fullScreen", full_screen_onClick);
			}
			else
			{
				stage["displayState"] = "normal";
			}
		}
	}

	private function hi_lo_onClick (event:MouseEvent):void
	{
		if (playing) stop();
		hiloButton.Click();
		source = base_url + "_" + hiloButton.state + ".flv";
		play();
	}

	public function SetBaseURL (url:String):void
	{
		base_url = url;
		source = base_url + "_" + hiloButton.state + ".flv";
	}

	public override function play ():void
	{
		super.play();
		ShowSpinner();
		playPressed = true;
	}

    public override function stop():void
    {
		super.stop();
		HideSpinner ();
        playPressed = false;
    }

	public function destroy ():void
	{
		HideSpinner ();
		super.destroyVideoPlayer();
		playheadSlider.value = 0;
		timerTextField.text = "";
		playPressed = false;
	}

	public function ShowSpinner ():void
	{
		var segs:Number = 20;
		var max:Number = 360;
		var inc:Number = max / segs;
		var alp:Number = 0.8;
		
		HideSpinner ();
		spinner_sprite = new UIComponent;

		for (var ang:Number=0; ang < max; ang += inc){
			if (alp > 0){
				var sprite:Sprite = new Sprite;
				sprite.graphics.beginFill (0xEEEEEE);
				sprite.graphics.moveTo (-2.5  ,20);
				sprite.graphics.lineTo (-4  ,30);
				sprite.graphics.lineTo (4  ,30);
				sprite.graphics.lineTo (2.5  ,20);
				sprite.graphics.lineTo (-2.5  ,20);
				sprite.graphics.endFill ();
				sprite.rotation = ang;
				sprite.alpha = alp;
				alp -= 1.0 / segs;
				spinner_sprite.addChild (sprite);}}

		spinner_sprite.x = videoPlayer.width / 2;
		spinner_sprite.y = videoPlayer.height / 2;
		spinner_sprite.addEventListener (Event.ENTER_FRAME, OnSpin);
		spinner_sprite.alpha = 0;
		spinner_sprite.scaleX = 0.5;
		spinner_sprite.scaleY = 0.5;
		spinner_alpha = -0.5;
		addChild (spinner_sprite);
	}

	public function OnSpin (e:Event):void
	{
		spinner_sprite.rotation -= 10;
		if (spinner_alpha < 1.0) spinner_alpha += 0.1;
		if (spinner_alpha > 0) spinner_sprite.alpha = spinner_alpha;
	}

	public function HideSpinner ():void
	{
		if (spinner_sprite){
			spinner_sprite.removeEventListener (Event.ENTER_FRAME, OnSpin);
			removeChild (spinner_sprite);}
		spinner_sprite = null;
	}
}
}
