package source.custom
{
	public class CustomTime
	{
		public var this_year:int;
		public var this_day:int;
		public var this_minute:int;

		// this is innacurate, but thats not important		
		static public var days_in_month:Array = [ 0,31,60,91,121,152,182,213,244,274,305,335,366 ];
		static public var month_names:Array = [ "Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec" ];

		public function Set (year:String, date:String, time:String):void
		{
			this_year = int (year);

    		var split:Array = date.split ("/");
    		var day:int = int (split[0]) - 1;
    		var month:int = int (split[1]) - 1;

			this_day = days_in_month[month] + day;

    		split = ValidateTime ("24", time).split (":");
			var hour:int = int (split[0]);
    		var minute:int = int (split[1]);

    		this_minute = hour * 60 + minute;
		}
		public function Difference (options:*, base:CustomTime):Number
		{
			var linear:Number;
			if (options.time_enable_year)
			{
				linear = base.this_year - this_year;
			}
			if (options.time_enable_date)
			{
				linear = linear * 366;
				linear = linear + (base.this_day - this_day);
			}
			if (options.time_enable_time)
			{
				linear = linear * 24 * 60;
				linear = linear + (base.this_minute - this_minute);
			}
			return linear;
		}
		public static function RoadString (options:*, base:CustomTime, time:Number) : String
		{
			var time_string:String = "";
			var year_inc:Number = 1;
			var day_inc:Number = 1;

			if (options.time_enable_time)
			{
				year_inc = 366 * 24 * 60;
				day_inc = 24 * 60;
			}
			else if (options.time_enable_date)
			{
				year_inc = 366;
			}
			if (options.time_enable_year)
			{
				var year:Number = Math.floor (time / year_inc);
				time_string = String (base.this_year + year);
				time = time - year * year_inc;
			}
			if (options.time_enable_date)
			{
				var day:Number = Math.floor (time / day_inc);
				time = time - day * day_inc;
				for (var i:int=days_in_month.length-1; i>=0; i--){
					if (day >= days_in_month[i]) break;}
				var j:int = day - days_in_month[i++] + 1;
				var string:String = "";
				string += String (j);
				string += " ";
				string += month_names [i];
				if (time_string.length) time_string += "<br>";
				time_string += "<font size=\"-1\">" + string + "</font>";
			}
			if (options.time_enable_time)
			{
				//object.time = String (year);
			}
			return time_string;
		}
		public static function SlideString (options:*, base:CustomTime, time:Number) : String
		{
			var time_string:String = "";
			var year_inc:Number = 1;
			var day_inc:Number = 1;

			if (options.time_enable_time)
			{
				year_inc = 366 * 24 * 60;
				day_inc = 24 * 60;
			}
			else if (options.time_enable_date)
			{
				year_inc = 366;
			}
			if (options.time_enable_year)
			{
				var year:Number = Math.floor (time / year_inc);
				time_string = String (base.this_year + year);
				time = time - year * year_inc;
			}
			return time_string;
		}
		public static function YearString (year:Number) : String
		{
			if (year < 0){
				if (year <= -1000000000) return String (Math.round (year / -100000000) / 10) + " BYA";
				if (year <= -1000000) return String (Math.round (year / -100000) / 10) + " MYA";
				if (year <= -8000){
					var ya:Number = Math.round ((2000 - year) / 100) * 100;
					var sya:String = String (ya);
					return sya.substr(0,sya.length-3) + "," + sya.substr(-3) + " YA";}
				return String (2000 - year) + " YA";}
			return String (year);
		}
		public static function TimeString (options:*, object:*) : String
		{
			var time_string:String = "";

			if (options.time_enable_date){
				if (options.time_enable_date) time_string += object.date;}

			else if (options.time_enable_time){
				time_string = object.time;}

			if (options.time_enable_year){
				if (options.time_enable_date) time_string += "/";
				time_string += YearString (object.year);}

			return time_string;
		}
    	public static function ValidateDate (date:String):String
    	{
    		if (!date.length) return "01/01";
    		var split:Array = date.split ("/");
    		var day:int = 1;
    		var month:int = 1;
    		if (split.length >= 1) day = int (split[0]);
    		if (split.length >= 2) month = int (split[1]);
    		if (day < 1 || day > 31) day = 1;
    		if (month < 1 || month > 12) month = 1;
			date = String(day);
			date += "/";
			date += String(month);
 			return date;
    	}
    	public static function ValidateTime (time_format:String, time:String):String
    	{
    		if (!time.length) return "0:00";
    		var ampm:String = " AM";
    		var split:Array = time.split (":");
    		var hour:int = 1;
    		var minute:int = 1;
    		if (split.length >= 1) hour = int (split[0]);
    		if (split.length >= 2)
    		{
    			var sin:String = split[1];
    			var sout:String = "";
    			for (var i:int=0; i<sin.length; i++){
					var a:String = sin.charAt(i);
					if (a >= '0' && a <= '9') sout += a;
					else if (a == 'p' || a == 'P'){ ampm = " PM"; break; }}
    			minute = int (sout);
    		}
    		if (hour < 0 || hour > 24) hour = 0;
    		if (minute < 0 || minute > 59) minute = 0;

			if (time_format == "12"){
				if (hour > 12){ hour -= 12; ampm = " PM";}}

			if (time_format == "24" && ampm == " PM"){
				hour += 12;}

			time = String(hour);
			time += ":";
			if (minute > 9) time += String(minute);
			else time += "0" + String(minute);

			if (time_format == "12"){
				time += ampm;}

 			return time;
    	}
	}
}
