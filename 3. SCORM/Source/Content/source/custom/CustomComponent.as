package source.custom
{
	import mx.core.*
	import mx.events.*;
	import mx.controls.*;
	import mx.rpc.events.*;
	import mx.collections.*;
	import flash.events.*;
	import flash.display.*;
/*
	Subclass of the default flex component base
	so that more variables can be added
*/ 
	public dynamic class CustomComponent extends UIComponent
	{
		public var fill_sprite : Sprite = null;
		public var text_sprite : Sprite = null;
		public var event_name : String = "";
		public var event_index : int = 0;
		public var event_xml : XML = null;
	}
}