package source.courses
{
	import flash.events.*;
	import flash.display.*;

	public class ElementTransitionEffectFade extends ElementTransitionEffect
	{
		public static var alpha_factor:Number = 0.12;

		public override function Apply (element:Element):Boolean
		{
			if (super.Apply (element)){
				sprite.addEventListener (Event.ENTER_FRAME, EnterFrame);
				sprite.alpha = 0;
				return true;}
			return false;
		}
		public function EnterFrame (event:Event):void
		{
			var alpha:Number = sprite.alpha;
			alpha = Math.min (1, alpha + alpha_factor);
			sprite.alpha = alpha;
			if (alpha == 1){
				sprite.removeEventListener (Event.ENTER_FRAME, EnterFrame);}
		}
	}
}
