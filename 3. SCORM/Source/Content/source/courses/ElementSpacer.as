package source.courses
{
	import flash.display.*;
	import source.general.*;

	public class ElementSpacer extends Element
	{
		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
			is_graphical = true;
			fixed_width = true;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			if (!element_sprite)
				element_sprite = new Sprite;
			element_width = GeneralConversions.ScaleToValueOrPercent (xml.@width, layout_width);
			element_height = GeneralConversions.ScaleToValueOrPercent (xml.@height, layout_width);
			return true;
		}
		public override function get can_enable ():Boolean { return false; }
	}
}
