package source.courses
{
	import flash.display.*;
	import flash.filters.*;
	import flash.events.*;
	import source.general.*;

	public class ElementPopup extends ElementTableColumn
	{
		public static var padding_width:int = 24;
		public static var padding_height:int = 24;
		public static var max_width_percent:int = 90;
		public static var close_button_url:String = "close.png";
		public static var close_button_width:int = 20;

		public var popup_sprite:Sprite;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
			SetEnabled (false);

			element_padding_width = padding_width;
			element_padding_height = padding_height;

			has_label = true;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			layout_width = Course.page_width * max_width_percent / 100;

			popup_sprite = new Sprite;
			popup_sprite.graphics.beginFill (0xF4F4F4, 0.5);
			popup_sprite.graphics.drawRect (0, 0, Course.page_width, Course.page_height);

			slide.ShowPopup (this);

			super.DoLayout (layout_width, layout_height);

			var inner_sprite:Sprite = new Sprite;
			inner_sprite.graphics.beginFill (0xFFFFFF);
			inner_sprite.graphics.lineStyle (1, 0xA0A0A0, 1, true);
			inner_sprite.graphics.drawRoundRect (0, 0, element_width, element_height, 12, 12);
			inner_sprite.filters = [new DropShadowFilter (1,45,0,0.5,8,8,1,4)];
			inner_sprite.x = (Course.page_width - element_width) / 2;
			inner_sprite.y = (Course.page_height - element_height) / 2;
			popup_sprite.addChild (inner_sprite);

			inner_sprite.addChild (element_sprite);
			element_sprite.x = 0;
			element_sprite.y = 0;

			var button_sprite:Sprite = new Sprite;
			var image_url:String = Course.ResolveMediaPath ("icons", close_button_url);
			var loader:Loader = GeneralLoader.LoadImage (image_url, element_width - close_button_width - 3, 4);
			button_sprite.addChild (loader);
			button_sprite.buttonMode = true;
			button_sprite.addEventListener (MouseEvent.CLICK, CloseHandler);
			inner_sprite.addChild (button_sprite);

			return false;
		}
		public override function RemoveFrom (parent:Sprite):void
		{
			slide.HidePopup (this);
		}
		public function CloseHandler (event:Event):void
		{
			SetEnabled (false);
			slide.HidePopup (this);
			slide.LayoutAndDraw();
		}
		public override function DocumentElement ():String
		{
			return DocumentBlock (group_array);
		}
	}
}