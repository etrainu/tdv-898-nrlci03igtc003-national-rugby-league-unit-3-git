package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import source.custom.*;
	import source.general.*;

	public class CourseDocSlide extends CourseSlide
	{
		public function CourseDocSlide (xml:XML)
		{
			super (xml);			
		}
		public function Build ():String
		{
			var output:String = "";

			var element_array:Array = [];
			var element_list:XMLList = page_xml.element;
			for each (var element_xml:XML in element_list){
				var element:Element = CreateElement (element_xml);
				element.Setup (element_xml, this);
				element_array.push (element);}

			for (var element_index:int=element_array.length-1; element_index>=0; element_index--){
				element = element_array[element_index];
				element.PrepareGroup (element_array, element_index);}

			for each (element in element_array){
				output += element.DocumentElement ();}

			return output;
		}
	}
}
