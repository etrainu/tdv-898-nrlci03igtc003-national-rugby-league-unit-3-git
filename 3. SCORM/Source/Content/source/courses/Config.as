package source.courses
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;

	public class Config extends EventDispatcher
	{
		[Bindable] public var username:String = "Developer " + Math.floor (Math.random() * 900 + 100);
		[Bindable] public var password:String = "";
		[Bindable] public var course_counter:int = 1;
		[Bindable] public var course_folder:String = "";
		[Bindable] public var course_server:String = "http://localhost:8080/AutoCourseServer";
		[Bindable] public var course_search:String = "";
		[Bindable] public var course_file:String = "";
		[Bindable] public var sound_search:String = "q#.mp3";
		public var recent_file_array:Array = [];
		public var page_width:int = 600;
		public var page_height:int = 600;
		public var course_xml:XML = null;

		public function Config (folder:String)
		{
			course_folder = folder;
		}
		public function Copy (source:Object):void
		{
			for (var property_name:* in source){
				var data:Object = source[property_name];
				if (this.hasOwnProperty(property_name)){
					this[property_name] = data;}}
		}
		public function AddRecentFile (filename:String):void
		{
			var i:int = recent_file_array.indexOf (filename);
			if (i >= 0) recent_file_array.splice (i, 1);
			recent_file_array.splice (0, 0, filename);
			if (recent_file_array.length > 40){
				recent_file_array.length = 40;}
		}
	}
}
