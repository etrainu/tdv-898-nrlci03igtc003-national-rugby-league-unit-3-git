package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import source.custom.*;
	import source.general.*;
	import source.courses.*;

	public class CourseSlide extends CoursePage
	{
		public var label_array:Array = [];
		public var main_column:ElementTableColumn;
		public var popup_element:ElementPopup;
		public var popup_sprite:Sprite;
		public var unload_array:Array;

		public function CourseSlide (xml:XML)
		{
			super (xml);			
		}
		public function ShowSlide():void
		{
			unload_array = [];
			var element_array:Array = [];
			var element_list:XMLList = page_xml.element;
			for each (var element_xml:XML in element_list){
				var element:Element = CreateElement (element_xml);
				element.Setup (element_xml, this);
				element_array.push (element);
				unload_array.push (element);}

			for (var element_index:int=0; element_index<element_array.length; element_index++){
				element = element_array[element_index];
				if (element.has_label) label_array.push (element);
				element.PrepareElement (element_array, element_index);}

			for (element_index=element_array.length-1; element_index>=0; element_index--){
				element = element_array[element_index];
				element.PrepareGroup (element_array, element_index);}

			for (element_index=0; element_index<element_array.length; element_index++){
				element = element_array[element_index];
				element.PrepareGroupElement (element_array, element_index);}

			main_column = new ElementTableColumn;
			main_column.group_array = element_array;
			main_column.FindGraphicalEnds();
			addChild (main_column.element_sprite = new Sprite);
			LayoutAndDraw ();
		}
		public function CreateElement (element_xml:XML):Element
		{
			var type:String = element_xml.@type;
			var element:Element = null;
			if (type == "button") element = new ElementButton;
			else if (type == "event") element = new ElementEvent;
			else if (type == "flash") element = new ElementFlash;
			else if (type == "heading") element = new ElementHeading;
			else if (type == "image") element = new ElementImage;
			else if (type == "group") element = new ElementGroup;
			else if (type == "paragraph") element = new ElementParagraph;
			else if (type == "list item") element = new ElementListItem;
			else if (type == "popup") element = new ElementPopup;
			else if (type == "panel") element = new ElementPanel;
			else if (type == "spacer") element = new ElementSpacer;
			else if (type == "sound") element = new ElementSound;
			else if (type == "timing") element = new ElementTiming;
			else if (type == "timer") element = new ElementEventTimer;
			else if (type == "transition") element = new ElementTransition;
			else if (type == "filter") element = new ElementFilter;
			else if (type == "table"){
				var layout:String = element_xml.@layout;
				if (layout == "column") element = new ElementTableColumn;
				else if (layout == "row") element = new ElementTableRow;}
			else if (type == "video") element = new ElementVideo;
			else element = new Element;
			return element;
		}
		public function LayoutAndDraw ():void
		{
			ElementListItem.bullet_counter = 0;
			main_column.DoLayout (Course.page_width, Course.page_height, true);
		}
		public function ShowAll (state:Boolean, refresh:Boolean):void
		{
			for each (var element:Element in label_array){
				Object(element).GroupEnable (state);}

			if (refresh) LayoutAndDraw ();
		}
		public function ShowLabel (label:String, state:Boolean, refresh:Boolean):void
		{
			for each (var element:Element in label_array){
				if (element.label == label){
					Object(element).GroupEnable (state);}}

			if (refresh) LayoutAndDraw ();
		}
		public function ShowPopup (popup:ElementPopup):void
		{
			if (popup_element == popup) return;
			HideActivePopup();
			GeneralSound.Stop();
			GeneralVideo.Stop();
			popup_element = popup;
			popup_sprite = popup.popup_sprite;
			if (popup_sprite) this.addChild (popup_sprite);
		}
		public function HidePopup (popup:ElementPopup):void
		{
			if (popup == popup_element)
				HideActivePopup();
		}
		public function HideActivePopup ():void
		{
			if (popup_element){
				GeneralSound.Stop();
				GeneralVideo.Stop();
				popup_element.SetEnabled (false);
				if (this.contains (popup_sprite))
					this.removeChild (popup_sprite);
				popup_element = null;
				popup_sprite = null;}
		}
		public override function UnloadAndStop ():void
		{
			if (unload_array){
				for each (var element:Element in unload_array){
					element.UnloadAndStop();}}
		}
	}
}
