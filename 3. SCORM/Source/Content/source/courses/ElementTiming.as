﻿package source.courses
{
	import flash.display.*;
	import source.general.*;
	import flash.events.*;
	import flash.utils.*;

	public class ElementTiming extends Element
	{
		public static var mute_skip_delay:Number = 0;

		public var time_index:int = 0;
		public var time_array:Array = [];
		public var index_array:Array = [];
		public var index_index:int = 0;
		public var start_index:int = 0;
		public var element_array:Array = [];
		public var timer:Timer;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
			
			allow_timing = true;
		}
		public function MuteSkip ():Boolean
		{
			return (String (xml.@mute_skip) == "true") && GeneralSound.IsMute();
		}
		public override function PrepareElement (array:Array, index:int):void
		{
			if (MuteSkip()) return;
			var count:int = xml.@count;
			if (!count) count = array.length;
			
			var times:String = xml.@delays;
			var split_array:Array = times.split(',');
			if (split_array.length){
				for (var i:int=0; i<count; i++){
					var value:String = "";
					if (i < split_array.length) value = split_array[i];
					else value = split_array[split_array.length - 1];
					var time:Number = Number(GeneralString.Trim (value));
					time_array.push (time);}}

			start_index = index + 1;
			for (i=start_index; count && i<array.length;i++){
				var element:Element = array[i];
				if (element.allow_timing){
					index_array.push (i - start_index);
					count--;}
				if (element is ElementTiming)
					break;}

			for (i=start_index; i<array.length;){
				element = array[i++];
				element_array.push (element);
				element.SetEnabled(false);
				if (element is ElementTiming)
					break;}

			index_array[0] = 0;
			index_array.push (i - start_index);
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			if (has_played) return false;
			SetEnabled (false);
			NextTimer ();
			has_played = true;
			return false;
		}
		public function NextTimer ():void
		{
			if (MuteSkip()){
				while (ShowNextElements());}
			
			if (time_index < time_array.length){
				var delay:Number = time_array[time_index++];
				if (delay >= 0){
					timer = new Timer (delay * 1000, 1);
					timer.addEventListener (TimerEvent.TIMER, TimerHandler);
					timer.start();}
				else TimerHandler (null);}
		}
		public function TimerHandler (event:TimerEvent):void
		{
			ShowNextElements ();
			slide.LayoutAndDraw();
			NextTimer ();
		}
		public function ShowNextElements ():Boolean
		{
			if (index_index < index_array.length - 1){
				var i:int = index_array[index_index];
				var end:int = index_array[index_index+1];
				for (;i<end; i++){
					var element:Element = element_array[i];
					element.SetEnabled (true);}
				index_index++;
				return true;}
			return false;
		}
		public override function UnloadAndStop ():void
		{
			if (timer) timer.stop();
		}
	}
}
