package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.custom.*;
	import source.general.*;

	public class OptionDragImage extends OptionDrag
	{
		public var image_loader:CustomLoader;
		public static var image_width:int = 90;
		public static var image_height:int = 90;

		public function OptionDragImage (xml:XML, down:Function, move:Function, drop:Function, box_width:int, box_height:int)
		{
			super (xml, box_width, box_height, down, move, drop);

			background_color = 0xFFFFFF;
			background_alpha = 1;

			var box:Sprite = new Sprite;
			var image_url:String = Course.ResolveMediaPath ("images", xml.image.@url);
			image_loader = GeneralLoader.LoadSizedImage (image_url, 0, 0, box_width, box_height);

			var base:Sprite = new Sprite;
			base.graphics.beginFill (background_color, background_alpha);
			base.graphics.drawRect (0, 0, box_width, box_height);
			box.addChild (base);

			box.addChild (image_loader);

			var border:Sprite = new Sprite;
			border.graphics.lineStyle (1, border_color, border_alpha, true);
			border.graphics.drawRect (0, 0, box_width, box_height);
			box.addChild (border);

			var text:String = String (xml.text);
			
			if (text.length){
				var text_sprite:CustomSprite = GeneralText.MakeText (text, text_size, text_color, 3, 2, box_width);

				var text_background:Sprite = new Sprite;
				base.graphics.beginFill (background_color, background_alpha);
				base.graphics.drawRect (0, 0, box_width, box_height);
				box.addChild (text_background);

				box.addChild (text_sprite);}

			box.buttonMode = true;
			this.addEventListener (MouseEvent.MOUSE_DOWN, OnMouseDown);
			addChild (box);
		}
	}
}
