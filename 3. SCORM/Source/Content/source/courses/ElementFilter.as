package source.courses
{
	import flash.display.*;
	import flash.filters.*;

	public class ElementFilter extends Element
	{
		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);
		}
		public override function PrepareElement (array:Array, index:int):void
		{
			var name:String = element_xml.@filter;
			var count_down:int = xml.@count;
			if (!count_down) count_down = -1;
			while (count_down && index < array.length){
				var element:Element = array[index++];
				if (element.allow_filter){
					var filter:Array = null;
					if (name == "image shadow" && element.is_image) filter = [new DropShadowFilter (1,45,0,0.5,6,6,1,4)];
					if (name == "text shadow" && element.is_text) filter = [new DropShadowFilter (2,45,0,0.3,2,2,1,1)];
					if (filter){
						element.element_filters = filter;
						count_down--;}}}
		}
	}
}
