package source.courses
{
	import flash.display.*;
	import source.general.*;
	import flash.events.*;
	import flash.media.*;
	import flash.net.*;

	public class ElementVideo extends Element
	{
		public var url_array:Array = [];
		public var video_scale:Number = 1;
		public var function_finish:Function;
		public var start_image_loader:Loader; 

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);

			var urls:String = _xml.@url;
			var array:Array = urls.split(',');
			for each (var url:String in array){
				var trim:String = GeneralString.Trim (url);
				var path:String = Course.ResolveMediaPath ("videos", trim);
				url_array.push (path);}

			video_scale = Number (xml.@scale) / 100;
			element_width = Number (xml.@width) * video_scale;
			element_height = Number (xml.@height) * video_scale;

			is_image = true;
			fixed_width = true;
			is_playable = true;
			is_graphical = true;
			allow_timing = true;
			allow_filter = true;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			if (!element_sprite){
				element_sprite = new Sprite;}

			ClearVideo();

			var start_image_url:String = xml.@image;
			var auto_start:Boolean = (start_image_url == "");

			if (auto_start) PlayVideo ();
			else {
				var url:String = Course.ResolveMediaPath ("images", start_image_url);
				start_image_loader = GeneralLoader.LoadImage (url);
				element_sprite.addChild (start_image_loader);
				element_sprite.addEventListener (MouseEvent.CLICK, ClickStart);
				element_sprite.buttonMode = true;}

			has_played = true;
			return true;
		}
		public function ClickStart (event:Event):void
		{
			element_sprite.buttonMode = false;
			element_sprite.removeEventListener (MouseEvent.CLICK, ClickStart);
			if (element_sprite.contains (start_image_loader))
				element_sprite.removeChild (start_image_loader);
			PlayVideo();
		}
		public function PlayVideo ():void
		{
			var video:Video = GeneralVideo.Play (url_array, true, null, function_finish);
			video.width = element_width;
			video.height = element_height;
			element_sprite.addChild (video);
		}		
		public override function SetEnabled (state:Boolean):void
		{
			super.SetEnabled (state);
			GeneralVideo.Stop();
			ClearVideo ();
		}
		public override function DocumentElement ():String
		{
			return DocumentComment ("[ Play video: " + url_array[0] + " ]");
		}
		public function ClearVideo ():void
		{
			if (element_sprite){
				while (element_sprite.numChildren){
					element_sprite.removeChildAt(0);}}
		}
	}
}
