package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.custom.*;
	import source.general.*;

	public class OptionDropWindow extends Option
	{
		public var box_width:int;
		public var box_height:int;
		public var box_label:String;
		public var text_sprite:CustomSprite;
		public var drop_array:Array = [];
		public var drag_top_x:int;
		public var drag_top_y:int;
		public var drag_pos_x:int;
		public var drag_pos_y:int;
		public var drag_bottom:int;
		public var label_height:int;
		public static var min_height:int = 68;
		public var fixed_height:int = 0;
		public var stacking_factor:Number = 1;
		public var drop_scale:Number = 1;

		public function OptionDropWindow (xml:XML, _width:int)
		{
			super (xml);
			box_width = _width;
			box_height = min_height;
			box_label = String (xml.text);

			text_sprite = GeneralText.MakeText (box_label, text_size, text_color, 0, 0, box_width-horz_margin*2);
			text_sprite.x = horz_margin;
			text_sprite.y = vert_margin;
			label_height = text_sprite.height + vert_margin * 2;
		}
		public function Render () : void
		{
			while (numChildren)
				this.removeChildAt(0);

			var box : Sprite = new Sprite;
			box.graphics.beginFill (background_color, background_alpha);
			box.graphics.drawRoundRectComplex (0, 0, box_width, label_height, corner_radius, corner_radius, 0, 0)
			box.graphics.endFill ();

			drag_top_x = drag_pos_x = box_gap;
			drag_bottom = drag_top_y = drag_pos_y = label_height + box_gap;
			for (var i:int=0; i<drop_array.length; i++){
				var drag_item:OptionDrag = drop_array[i];
				PlaceDroppedItem (drag_item);}
			box_height = drag_bottom;

			if (box_height < min_height) box_height = min_height;
			if (fixed_height) box_height = fixed_height;

			box.graphics.lineStyle (1, border_color, border_alpha, true);
			box.graphics.drawRoundRect (0, 0, box_width, box_height, corner_radius, corner_radius);
			box.graphics.moveTo (0, label_height);
			box.graphics.lineTo (box_width, label_height);
			box.buttonMode = true;

			box.addChild (text_sprite);
			addChild (box);
		}
		public function PlaceDroppedItem (item:OptionDrag):void
		{
			if (drag_pos_x + item.width > box_width){
				drag_pos_x = drag_top_x;
				drag_pos_y = drag_bottom;}
			item.desired_x = this.x + drag_pos_x;
			item.desired_y = this.y + drag_pos_y;
			item.desired_scale = drop_scale;
			drag_pos_x += item.box_width / stacking_factor + box_gap;
			drag_bottom = drag_pos_y + item.box_height + box_gap;
		}
		public function RemoveOption (option:Option):void
		{
			var i:int = drop_array.indexOf (option);
			if (i >= 0) drop_array.splice (i,1);
		}
	}
}
